#include <chrono>
#include <iostream>
#include <mpi.h>

void print_matrix(int *matrix, int m, int n) {
    for (size_t i = 0; i < m; ++i) {
        for (size_t j = 0; j < n; ++j) {
            std::cout << matrix[i * n + j] << "\t";
        }
        std::cout << std::endl;
    }
}


int main(int argc, char *argv[]) {

    auto start_time = std::chrono::steady_clock::now();

    auto a_rows = static_cast<int>(std::strtol(argv[argc - 4], nullptr, 0));
    auto a_cols = static_cast<int>(std::strtol(argv[argc - 3], nullptr, 0));
    auto b_rows = static_cast<int>(std::strtol(argv[argc - 2], nullptr, 0));
    auto b_cols = static_cast<int>(std::strtol(argv[argc - 1], nullptr, 0));

    // Matrizen werden 1D gespeichert, da MPI zum scattern
    // zusammenhaengend belegten Speichern braucht.
    int *matrix_a = nullptr;
    auto *matrix_b = new int[b_rows * b_cols];
    int *matrix_result = nullptr;

    int world_size, world_rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Thread #0 initialisiert alle Daten:
    if (world_rank == 0) {

        if (a_cols != b_rows) {
            printf("ERROR: Dimensionen passen nicht!\n");
            exit(EXIT_FAILURE);
        }

        if (a_rows % world_size != 0) {
            printf("ERROR: Matrizengröße passt nicht zur Threadanzahl!\n");
            exit(EXIT_FAILURE);
        }

        // Fuelle erste Matrix mit Zahlen:
        matrix_a = new int[a_rows * a_cols];
        for (int i = 0; i < a_rows * a_cols; ++i) {
            matrix_a[i] = i;
        }

        // Fuelle zweite Matrix mit Zahlen:
        for (int i = 0; i < b_rows * b_cols; ++i) {
            matrix_b[i] = i;
        }

        // Reserviere Speicher für die dritte Matrix:
        matrix_result = new int[a_rows * b_cols];

        //print_matrix(matrix_a, a_rows, a_cols);
        //std::cout << "\t  X  \t" << std::endl;
        //print_matrix(matrix_b, b_rows, b_cols);
    }

    // Wir verteilen Matrix A zeilenweise an die Threads:
    int sub_cols = a_cols;
    int sub_rows = a_rows / world_size;
    int sub_size = sub_cols * sub_rows;

    auto *sub_a = new int[sub_size];
    MPI_Scatter(matrix_a, sub_size, MPI_INT, sub_a,
                sub_size, MPI_INT, 0, MPI_COMM_WORLD);

    // Jeder Prozess muss Matrix B komplett kennen:
    MPI_Bcast(matrix_b, b_rows * b_cols, MPI_INT, 0, MPI_COMM_WORLD);

    // Reserviere den lokalen Teil der Ergebnismatrix:
    // Groeße der Ergebnismatrix durch Anzahl der Threads
    int sub_result_rows = a_rows / world_size;
    int sub_result_size = sub_result_rows * b_cols;
    auto *sub_result_matrix = new int[sub_result_size]();

    int position = 0;

    // Multipliziere nun ganz normal unsere Teilmatrix mit Matrix B:
    for (size_t i = 0; i < sub_result_rows; ++i) {
        for (size_t j = 0; j < b_cols; ++j) {
            for (size_t k = 0; k < b_rows; ++k) {
                sub_result_matrix[position] +=
                        sub_a[(i * a_cols + k)] *
                        matrix_b[(k * b_cols + j)];
            }
            position++;
        }
    }

    // Sammle die Ergebnismatrizen in unserer oben reservierten Matrix 'result':
    MPI_Gather(sub_result_matrix, sub_result_size, MPI_INT,
               matrix_result, sub_result_size, MPI_INT, 0, MPI_COMM_WORLD);

    // Gib das Ergebnis aus:
    if (world_rank == 0) {

        auto end_time = std::chrono::steady_clock::now();

        //std::cout << "\t  =" << std::endl;
        //print_matrix(matrix_result, a_rows, b_cols);

        std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(
                end_time - start_time).count() << std::endl;

        delete[] matrix_result;
        delete[] matrix_a;
    }

    delete[] sub_a;
    delete[] matrix_b;
    delete[] sub_result_matrix;

    MPI_Finalize();
    exit(EXIT_SUCCESS);
}