cmake_minimum_required(VERSION 3.12)
project(OpenMPI_GEMM)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "-pthread -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -L/usr/lib/openmpi -lmpi_cxx -lmpi -Os")

add_executable(OpenMPI_GEMM main.cpp)