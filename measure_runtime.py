#!/usr/bin/python3
import subprocess

if __name__ == "__main__":

    num_runs = 100
    matrix_a_rows = 1000
    matrix_a_cols = 1000
    matrix_b_cols = 1000

    current_val_openmpi = 0
    current_val_mpich = 0

    print("Testing {}x{} * {}x{} matrices for {} runs...".format(matrix_a_rows, matrix_a_cols, matrix_a_cols, matrix_b_cols, num_runs))
    print("Testing OpenMPI...")

    # OpenMPI
    for i in range(num_runs):
        current_number = subprocess.run("/usr/bin/mpirun -np 4 /home/lars/Desktop/Uni/Progs/C++/OpenMPI_GEMM/cmake-build-debug/OpenMPI_GEMM {} {} {} {}".
                                        format(matrix_a_rows, matrix_a_cols, matrix_a_cols, matrix_b_cols), shell=True, stdout=subprocess.PIPE)
        current_number = int(current_number.stdout.decode('utf-8'))
        current_val_openmpi = current_val_openmpi + current_number
        print("{}. run: {}ms".format(i, current_number))

    current_avg_openmpi = current_val_openmpi / num_runs

    print("Testing MPICH...")

    # MPICH
    for i in range(num_runs):
        current_number = subprocess.run("/opt/mpich/bin/mpirun -np 4 /home/lars/Desktop/Uni/Progs/C++/MPICH_GEMM/cmake-build-debug/MPICH_GEMM {} {} {} {}".
                                        format(matrix_a_rows, matrix_a_cols, matrix_a_cols, matrix_b_cols), shell=True, stdout=subprocess.PIPE)
        current_number = int(current_number.stdout.decode('utf-8'))
        current_val_mpich = current_val_mpich + current_number
        print("{}. run: {}ms".format(i, current_number))

    current_avg_mpich = current_val_mpich / num_runs

    print("OpenMPI-Avg: {}ms\nMPICH-Avg: {}ms".format(current_avg_openmpi, current_avg_mpich))
